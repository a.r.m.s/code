def log_watcher(log_file, arms, child_channels) 

    require 'rb-inotify'

    # run through the file as it is up front to see if one has already come through
    arms.logger.info("Looking through #{log_file} to see if some were already created...")
    File.open(log_file).read.split("\n") do |line|
        child_channels.any? { |channel| child_channels -= channel if line.include? channel }
        arms.logger.info("Repo metadata creation successful for: #{line[/.*'(.*)'.*/, 1]}")
    end

    arms.logger.info("Tailing #{log_file} to watch for successful creation of child channels")
    arms.logger.debug("All new child channels:\n#{child_channels}")
    File.open(log_file) do |file|
        file.seek(0, IO::SEEK_END) 
        queue = INotify::Notifier.new
        queue.watch(log_file, :modify) do
            line = file.read
            if line =~ /Repository metadata generation for 'tyson-.*' finished/
                arms.logger.info("Repo metadata creation successful for: #{line[/.*'(.*)'.*/, 1]}")
                child_channels -= ["#{line[/.*'(.*)'.*/, 1]}"]
                arms.logger.debug("All new child channels:\n#{child_channels}")
                return if child_channels == []
            end
        end
        queue.run
    end  

end

