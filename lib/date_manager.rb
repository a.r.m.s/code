def date_manager(suse_manager_object, arms)
    
    require 'date'
    
    current_custom_base_channels = suse_manager_object.list_custom_base_channels()
    exceptions = arms.configs['always_retain_these_distributions']

    # there's nothing to delete or manage
    return if current_custom_base_channels == []

    arms.logger.debug("All current Custom channels: #{current_custom_base_channels}")
    
    existing_dates = current_custom_base_channels.delete_if { |channel| channel !~ /custom-\d.*/ }.map { |channel| channel[/custom-(\d+-\d+-\d+.\d+-\d+)_.*/,1] }.map { |date| DateTime.strptime(date, arms.configs['time_format']) }.uniq!.sort
    arms.logger.debug("Current dates: #{existing_dates}")

    unless exceptions == nil
        arms.logger.debug("Exceptions pulled from configuration file: #{exceptions}")
        exceptions = exceptions.map { |distro| distro[/Custom-(\d+-\d+-\d+.\d+-\d+)_.*/,1] }.map { |date| DateTime.strptime(date, arms.configs['time_format']) }.uniq! 

        dates_to_consider = existing_dates - exceptions
        arms.logger.info("Set difference between existing dates and exceptions (only considering these dates to check for removal): #{dates_to_consider}")
    else
        dates_to_consider = existing_dates
        arms.logger.warn("NO EXCEPTIONS FOUND; considering all dates for possible removal.")
    end
    
    dates_to_keep = existing_dates.length >= arms.configs['max_num_dates_to_keep'] ? existing_dates [-1*arms.configs['max_num_dates_to_keep']..-1] : existing_dates
    arms.logger.debug("Dates being kept: #{dates_to_keep}")

    dates_to_consider.each do |date|
        unless dates_to_keep.include? date
            arms.logger.warn("Deleting distributions and channels corresponding to this date: #{date.strftime(arms.configs['log_time_format'])}")
            suse_manager_object.delete_channels(date.strftime(arms.configs['time_format']))
        end
    end
    
end
