def log_rotate(log_file_path)

    require 'zlib'
    require 'fileutils'

    dir = File.expand_path(File.dirname(log_file_path))
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    if File.exist? log_file_path
        last_time_stamp = File.open(log_file_path) { |file| file.readline }.scan(/\d/).insert(8, '-')[0..15].join('')

        zip_file = "#{File.expand_path(File.dirname(log_file_path))}/arms-#{last_time_stamp}.log.gz"
        Zlib::GzipWriter.open(zip_file) do |gz|
            gz.write IO.binread(log_file_path)
        end

        File.delete(log_file_path)
    end

end
