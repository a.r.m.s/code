#!/usr/bin/ruby

require 'git'

require_relative 'SuseManager'
require_relative 'Channel'
require_relative 'repobuilder'
require_relative 'ARMS'
require_relative 'date_manager'
require_relative 'log_watcher'
require_relative 'file_manager'

# ~~ Start up ~~~~~~~~~~~~~~~~~~~~~~~~ #
arms = ARMS.new()
arms.logger.info("Checked log rotation...")

MAX_ERROR_COUNT = 3

CURRENT_DATE = Time.now.strftime(arms.configs['time_format'])
arms.logger.info("Building repos with this time: #{CURRENT_DATE}")

suma = SuseManager.new(arms.configs['suse_manager_api_url'], ARGV[0], ARGV[1])
arms.logger.info("Logging into #{arms.configs['suse_manager_api_url']} as admin")

all_new_custom_child_channels = []
# ~~ Start up ~~~~~~~~~~~~~~~~~~~~~~~~ #

# move the current repos to a retained folder
file_manager(arms)

# trim old dates from SUMA
#date_manager(suma, arms)

suse_base_channels = suma.list_suse_base_channels(arms.configs['prepend_channel_label'], arms.configs['excluded_base_channels'])
arms.logger.debug("All SUSE base channels: #{suse_base_channels}")

error_count = 0
suse_base_channels.each do |base_channel|

    begin 
        arms.logger.info("Beginning: #{base_channel}")
        arms.logger.info("Trimming off -x86_64")
        trimmed_base_channel = base_channel.sub('-x86_64', '') # remove the x86_64 for cleaner names
        arms.logger.debug("Trimmed channel: #{trimmed_base_channel}")

        # skip this iteration entirely if it's an excluded channel/OS
        if !arms.configs['excluded_base_channels'].nil? && arms.configs['excluded_base_channels'].include?(trimmed_base_channel) 
            arms.logger.info("#{trimmed_base_channel}: skipping this iteration; excluded base channel")
            next
        end

        # create the scaffolding for the new channel
        new_base_channel_label = "#{arms.configs['prepend_channel_label']}-#{CURRENT_DATE}_#{trimmed_base_channel}"
        arms.logger.info("New base channel label: #{new_base_channel_label}")
        new_custom_base_channel = Channel.new(new_base_channel_label, new_base_channel_label, arms.configs['default_channel_summary'], '', 'sha256') # leave parent label blank, this is a base channel
         
        # make the clone - incorporate the scaffolding
        suma.clone_channel(base_channel, new_custom_base_channel)
        arms.logger.info('Completed cloning based channel.')

        # now get the child channels
        # append these to the newly created channel
        arms.logger.info('Cloning all child channels and appending them to the new custom base channel...')
        arms.logger.debug("Child channels: #{suma.list_child_channels(base_channel)}")
        suma.list_child_channels(base_channel).each do |child_channel| 
            suma.clone_channel(child_channel['label'], Channel.new("#{arms.configs['prepend_channel_label']}-#{CURRENT_DATE}-#{child_channel['name']}", "#{arms.configs['prepend_channel_label']}-#{CURRENT_DATE}_#{child_channel['label'].sub('-x86_64','')}", arms.configs['default_channel_summary'], new_base_channel_label, 'sha256'))
            all_new_custom_child_channels << "#{arms.configs['prepend_channel_label']}-#{CURRENT_DATE}_#{child_channel['label'].sub('-x86_64','')}"
        end

        # create the distribution from the new base channel
        arms.logger.info("Creating new distribution: #{new_base_channel_label.sub(arms.configs['prepend_channel_label'], arms.configs['prepend_distro_label'])}")
        suma.create_distribution("#{new_base_channel_label.sub(arms.configs['prepend_channel_label'], arms.configs['prepend_distro_label'])}", new_base_channel_label, arms.configs['os_specific_info'][trimmed_base_channel])

    rescue XMLRPC::FaultException => e

        error_count += 1
        arms.logger.debug("Error Counter = #{error_count}")

        if error_count >= MAX_ERROR_COUNT
            arms.logger.error(e.faultString)
            arms.logger.error('Encountered weird XMLRPC error from SUMA!') 
        else
            arms.logger.warn("Received this error: #{e.faultString}")
            arms.logger.info("To fix this error, usually wait a couple minutes and run again. We think this error means that SUMA was doing something the DB in the background when the script was running.")
            case error_count
            when 1
                arms.logger.info('Waiting 2 minutes before retrying current loop iteration...')
                arms.logger.warn('Cleaning up all most recently created channels from this loop iteration. This only deletes the most recently created child channels and base channel.')
                suma.list_child_channels(new_base_channel_label).each do |channel|
                    suma.delete_channel_by_name(channel['label'])
                end
                suma.delete_channel_by_name(new_base_channel_label)
                sleep 180
                arms.logger.info('Retrying loop iteration now...')
                redo
            when 2
                arms.logger.warn('Received this error twice now... Rather than redo the current iteration, restarting the whole loop.')
                arms.logger.warn('Cleaning up all most recently created distributions and channels from this run - going to try again.')
                suma.delete_channels(CURRENT_DATE)
                arms.logger.info('Waiting 5 minutes before restarting...')
                sleep 300
                arms.logger.info('Restarting entire loop.')
                retry
            end
        end
            
    end

    # call the script that will build the file
    arms.logger.info('Building repository files from newly created distribution')
    repobuild(CURRENT_DATE, trimmed_base_channel, suma.list_child_channels(new_custom_base_channel['label']), arms) 

end    

#log_watcher(arms.configs['taskomatic_log_path'], arms, all_new_custom_child_channels)

git = Git.open(arms.configs['repos_project'], :log => arms.logger)
git.add(arms.configs['repos_project'])
git.commit('built new repos')
git.push('origin', git.branch(arms.configs['git_staging_branch_name']))

