def file_manager(arms)

    require 'fileutils'

    stable_repos_dir = "#{arms.configs['repos_project']}/repos/linux/files/stable"

    stable_repo_files =  Dir.entries(stable_repos_dir).delete_if { |file| file.start_with? '.' }

    unless stable_repo_files.all? { |s| s.start_with?('.') } || stable_repo_files.nil? || stable_repo_files == []
        date, time = File.read("#{stable_repos_dir}/#{stable_repo_files[-1]}").split("\n")[0].split(' ')[1].split('.')   
        time.sub! '-', ':'

	directory_timestamp_name = FileUtils::mkdir_p("#{arms.configs['repos_project']}/repos/linux/files/#{date}\ #{time}/")[0]
        FileUtils.mv Dir.glob("#{stable_repos_dir}/*.repo"), directory_timestamp_name
    end

end
