class ARMS

    require 'yaml'
    require 'logger'

    require_relative 'log_rotate.rb'

    attr_reader :configs, :logger

    def initialize()

	@configs = YAML.load_file("#{File.expand_path(File.dirname(__FILE__))}/../arms-conf.yml") 
        @configs['time_format'] = '%m-%d-%Y.%H-%M'
        @configs['log_time_format'] = '%m/%d/%Y %H:%M:%S:%L'

	log_rotate(@configs['log_file_path'])

        if @configs['log_file_path'].nil?
            @logger = Logger.new(STDOUT)
        else
            @logger = Logger.new(@configs['log_file_path'])
        end

        @logger.datetime_format=(@configs['log_time_format']) unless @configs['log_time_format'].nil?

        case @configs['log_level']
        when 'WARN' || 'warn'
            @logger.level = Logger::WARN
        when 'DEBUG' || 'debug'
            @logger.level = Logger::DEBUG
        else
            @logger.level = Logger::INFO
        end

    end

end
