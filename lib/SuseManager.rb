require 'xmlrpc/client'
require_relative 'Channel'

class SuseManager

    def initialize(url, api_user, api_pass)
        @client =  XMLRPC::Client.new2(url)
        @key = @client.call('auth.login', api_user, api_pass)
    end

    def list_suse_base_channels(prepend_channel_label, excluded_base_channels)
	    suse_base_channels = []
	    channels = @client.call('channel.listSoftwareChannels', @key)
        channels.each do |channel|
            next if channel['label'].start_with? prepend_channel_label
            next if channel['parent_label'] != ''
            next if excluded_base_channels.include? channel['label']
            suse_base_channels << channel['label']
        end
        suse_base_channels
    end

    def list_custom_base_channels(prepend_channel_label)
        custom_base_channels = []
	    channels = @client.call('channel.listSoftwareChannels', @key)
        channels.each do |channel|
            next if channel['parent_label'] != ''
            custom_base_channels << channel['label'] if channel['label'].start_with? prepend_channel_label
        end
        custom_base_channels
    end

    def list_child_channels(channel_label)
        @client.call('channel.software.listChildren', @key, channel_label)
    end

    def create_distribution(distro_name, base_channel_label, os_info)
        @client.call('kickstart.tree.create', @key, distro_name, os_info['iso'], base_channel_label, os_info['install_type'])
    end

    def clone_channel(channel_label_to_clone, new_channel)
        @client.call('channel.software.clone', @key, channel_label_to_clone, new_channel, false)  
    end

    def associate_repo(channel_label, repo_label)
        @client.call('channel.software.associateRepo', @key, channel_label, repo_label)
    end

    def delete_channels(date)
        #`spacecmd --yes distribution_delete SZ-#{date}*`         
        #`spacecmd --yes softwarechannel_delete SZ-#{date}*`         
    end

    def delete_channel_by_name(channel_label)
        #`spacecmd --yes distribution_delete #{channel_label}`         
        #`spacecmd --yes softwarechannel_delete #{channel_label}`         
    end
end
