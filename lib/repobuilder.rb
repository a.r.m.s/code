def repobuild(current_date, new_base_channel, new_child_channels, arms)

    repo_file_path = "#{arms.configs['repos_project']}/repos/linux/files"

    arms.logger.info("Creating file #{repo_file_path}/stable/#{new_base_channel}.repo")
    File.open("#{repo_file_path}/stable/#{new_base_channel}.repo", 'w') do |file|

        file.write("\#\# #{CURRENT_DATE}\n\n")

        new_child_channels.each do |channel|
            file.write("[#{channel['label'].split('_')[1]}]\n")
            file.write("name=#{channel['label'].split('_')[1]}\n")
            arms.configs['blacklist_labels'].include?(channel['label'].split('_')[1]) ? file.write("enabled=0\n") : file.write("enabled=1\n") unless arms.configs['blacklist_labels'].nil?
            file.write("gpgcheck=0\n")
            file.write("autorefresh=0\n")
            file.write("baseurl=http://"+arms.configs['suse_manager_hostname']+"/ks/dist/child/#{channel['label']}/#{arms.configs['prepend_distro_label']}-#{CURRENT_DATE}_#{new_base_channel}\n")
            file.write("type=rpm-md\n")
            file.write("\n")
        end

        file.write("[#{new_base_channel}-ISO]\n")
        file.write("name=#{new_base_channel}-ISO\n")
        file.write("gpgcheck=0\n")
        file.write("autorefresh=0\n")
        file.write("baseurl=http://"+arms.configs['suse_manager_hostname']+"/pub/media/#{arms.configs['os_specific_info'][new_base_channel]['iso'].split('/')[-1]}/\n")
        file.write("type=rpm-md\n")

    end
end
