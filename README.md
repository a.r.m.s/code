# ARMS
### Automated Repository Management System (SUSE Manager)

### Infrastructure Setup:
1. SUSE Manager installed with repos synced down
2. Separate Salt Master
3. Gitlab Server
4. Gitlab Runner (docker) registered to repository project (explained below)
5. Salt Minions of SUSE/RHEL flavor (depending on what's synced in SUSE Manager)

### Some Notes Before Starting:

1. This still only works for x86_64, any other arch's will need to be blacklisted in the configuration file
2. ARMS is still in it's early stages... Forgive me for my errors
3. Next steps: RPM installer, Auto-Magic merge requests via Gitlab API 

### What it Do and How to Use:
Basically, this is for people/organizations that don't want SUSE Manager to be their Salt Master. Maybe they've already got one or more Salt Masters..?

ARMS does the following:
  - Use SUMA api to logically create Distributions and Channels within SUMA to make repos publically available at a URL
  - Output the actual repo files on a per-distro-version basis
  - Use Gitlab to test the newly created repos with Gitlab CI and Docker containers
  - On success, use Gitlab api to create a merge request to master
  - Do this repeatedly, silently (with logs and log rotation), and indefinitely
  - Use SaltStack to distribute repo files once tested and merged into master

Once you've got it all set up and it's running smoothly, your repo files are pulled from the master branch of your project in git (setup explained below) via salt. The salt command on the Salt Master is:

```bash
salt '*' repos.linux
```

This will drop a single repo file on your minions titled 'ARMS-Latest.repo' which will be used for ALL of your updates :)

The salt state also currently deletes all contents in the repository directory of the host. There is a way to add exceptions to not remove everything, but that's a version 2 code add-on...

New repos are generated and stored in a 'staging' branch of the repos project in git. You'll have to merge the new repos into master for the Salt Master to be able to view them. I highly advise turning on 'auto merge on successful pipeline' in Gitlab project settings! This way should the business say they're okay and ready to patch, all you have to do is click the merge button, and boom - new repos! (Go devops!)

### Configuration Setup:
- In Gitlab, make a project called Repos (or whatever you want to call it)
- On the SUSE Manager host, you'll have to ``createrepo`` with the ``.iso`` files under ``/srv/www/htdocs/pub/somewhere`` for each of the distros you plan to use. We found it generally better to also have the base ISOs available as repositories.
- On the Salt Master, clone the ARMS project (I typically install this in `/srv`)

```bash
cd /srv
git clone https://gitlab.thebonds.tech/open-source/arms/code.git
```

- Install the `git` gem as the only dependency: `gem install git`
- Clone the repository project (it's separate)
  - This can be installed anywhere - all repo files will be kept here
  - (I also happened to put it in /srv)

```bash
cd /srv
git clone https://gitlab.thebonds.tech/open-source/arms/repos.git
```

- **DELETE ALL CURRENT REPO FILES** Otherwise, you'll get all the repo files _I've_ been generating personally. (I haven't fixed it to wipe them on install somehow yet)
  -  Remove everything in `Repos/repos/linux/files/stable/*` and anything in `Repos/repos/linux/files/*` - BESIDES `Repos/repos/linux/files/stable` (the stable directory needs to be there)
  - Change the git url for this project to the ssh git url of the project you created 

```bash
git remote set-url --push origin git@gitlab.example.com:USER/PROJECT_NAME.git
```

- Once you've changed the git url, you'll be **required** to checkout a new branch from master. I suggest using the name 'staging' - this is where all your new repo files will be tested before being merged into master branch for distribution.

```bash
git checkout -b staging
```

- Generate root ssh keypair on SaltMaster

```bash
ssh-keygen -o -b 4096 
# accept defaults
```

- Take the pub key you just generated and add it as a deploy key in Gitlab for the project you created:
  - Navigate to the project in Gitlab > Settings > Repository > Deploy Keys
  - This allows for automated `git push` when new repos are generated from ARMS
- Edit the Salt Master configuration (or add one in `/etc/salt/master.d/`) use the project you created as a gitfs for salt
  - Install `python-pygit2` (if it isn't already installed) on the Salt Master
  - Uncomment `gitfs_provider` - leave as `pygit2`
  - Uncomment and change `gitfs_pubkey` and `gitfs_privkey` to the paths for each respectively (the keys you generated)
  - Uncomment `gitfs_remotes` and add the git ssh url from the project you made
  - Restart the salt-master process
  - Run: `salt-run fileserver.update` (you should see `True` as output)
  - Verify the it's working by running: `salt-run fileserver.file_list` - you should be able to see `repos/linux/init.sls` among other files and directories in the output
- Create a dummy user for API calls in SUSE Manager
  - This user need to be a **Configuration Adminstrator** and **Channel Administrator**
  - Do NOT check read only - this user will definitely be writing :)
- Back on the Salt Master, install the systemd service and timer files
  - Typically, these can be moved to `/usr/lib/systemd/system/` (I know, I need to make an RPM install instead of all this...)
  - **EDIT THE `.service` FILE AND UPDATE THE `ExecStart` COMMAND WITH YOUR SUSE MANAGER USER/PASS AS PARAMATERS TO THE FUNCTION** (this hasn't been fixed as of this release...)

```
[Service]
ExecStart=/usr/bin/ruby /srv/ARMS/lib/main.rb USER PASSWORD
```

- Back to the Gitlab project created way back when:
  - Navigate to your account settings > Access Tokens - generate yourself an access token with **api access** (and **sudo** - may not be required)
  - Navigate to the Project > Settings > CI/CD > Secret Variables and create a variable PRIVATE_TOKEN with the value set to your newly created access token
- **Pat yourself on the back for doing what an RPM installer should do most of for you** (plus some more)

### Editing the ARMS Configuration File:

The ``arms-conf.yml`` file contains configuration settings for ARMS and **MUST** be edited according to specific environments! Below is a posting of the config file with **EXAMPLE** values:

```
# Login URL
suse_manager_hostname: 'susemanager.domain.name'
suse_manager_url: 'https://susemanger.domain.name'
suse_manager_api_url: 'http://susemanager.domain.name/rpc/api'

# Log file location and level
log_file_path: /var/log/ARMS/arms.log
log_level: 'DEBUG'

# Maximum number of dates to retain in SUMA, excluding the exceptions (below)
# Exceptions are distributions, in list format
# All base/child channels and repo files will be kept under the distribution
#
## THESE ARE DISABLED RIGHT NOW!!
max_num_dates_to_keep: 3
always_retain_these_distributions:

# Labels used for differentiation between SCC repos and custom repos
# Usually, prepend_distro is just caps prepend_channel
# IF THESE ARE CHANGED AFTER A FEW RUNS, ALL PREVIOUS REPOS
# WILL BE WIPED CLEAN
prepend_channel_label: 'companyname'
prepend_distro_label: 'CompanyName'

# Base channel labels to ignore in SUMA
# These are OS's that SUMA might pull down nightly that don't need cloned
# A listing of what channels are available can be found using 'spacecmd softwarechannel_listbasechannels' on the SUSE Manager host
excluded_base_channels:
    - sles11-sp4-pool-i586 # example - only supports x86_64 for now, so exclude

# These will be included in the repo file, but have enabled=0
# Currently, these disable the SUSE Manager version of salt
# These are child channels, not base channels
blacklist_labels:

# Channel summary for when we clone channels
# Summary can be found in SUMA UI after clicking channel
# Put legal jargon here if needed
default_channel_summary: 'This channel was auto-generated by ARMS.'

# Git directories for testing and latest
repos_project: '/srv/Repos'
install_dir: '/srv/ARMS' # currently unused, don't worry about this for now
git_staging_branch_name: 'staging' 

# branch name to test files in
git_branch_name: 'testing' # currently unused, don't worry about this for now

# OS specific information - names are derived from SUMA base channel labels (stripped of '-x86_64')
# Once new OS is added, just add its ISO 
# Salt Repos: SUMA > Software > Channels > Manager Software Channels > Manage Repositories
# install_type is a SUMA-ism; we aren't really sure what these are
os_specific_info:
    'sles11-sp4-pool':
        iso: '/srv/www/htdocs/pub/media/SLES-11-SP4-DVD-x86_64-GM-DVD1'
        install_type: 'sles11generic'
    'sles12-sp1-pool':
        iso: '/srv/www/htdocs/pub/media/SLE-12-SP1-Server-DVD-x86_64-GM-DVD1'
        install_type: 'sles12generic'
    'sles12-sp3-pool':
        iso: '/srv/www/htdocs/pub/media/SLE-12-SP3-Server-DVD-x86_64-GM-DVD1'
        install_type: 'sles12generic'
```

- Should you need to support a new OS, all you'll have to do is sync it down in SUSE Manager and then update the ``os_specific_info`` lines for the new OS.

### Final Glue!

Okay, to get it all up and running after setup, there's just a few more steps...

- Register your gitlab runner to the Repos project you created (Project Settings > CI/CD > Runners) 
- If you need RHEL support at all in SUSE Manager, uncomment that section from the ``.gitlab-ci.yml`` in the Repos project you created
- You'll need access to a custom container I wrote for making the gitlab API calls for generating merge requests automatically
  - To do this, you'll just need to run ``docker login https://registry.thebonds.tech`` on your gitlab runner host. This will prompt you for a password, which you should be able to use an API token I grant you (it'll store this value in plain text on my server, so using your actual password should be avoided!)

### Test it out!

On the Salt Master, run ``ruby /srv/ARMS/lib/main.rb <SUMA API USER> <SUMA API PASS>`` at any time to test it all out!

### KNOWN ISSUES

The Gitlab API auto merging stuff will say it succeeded, but it actually doesn't. No idea what's up there. I'm fighting curl, but need more sleep.






